package io.konstantinzolotov.data

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import io.konstantinzolotov.domain.models.*
import io.konstantinzolotov.domain.repositories.ContactsRepository
import kotlinx.coroutines.experimental.*
import javax.inject.Inject


class ContactsRepositoryImpl @Inject constructor(private val context: Context) : ContactsRepository {

    override fun getAll(): Deferred<List<Contact>> = GlobalScope.async(context = Dispatchers.IO) {
        getAllContacts()?.let(::IterableCursor)?.map { contactCursor ->
            val id = contactCursor.getStringValue(ContactsContract.Data.LOOKUP_KEY)
            val name = contactCursor.getStringValue(ContactsContract.Data.DISPLAY_NAME_PRIMARY)
            val thumbnailUri = contactCursor.getStringValueOrNull(ContactsContract.Data.PHOTO_THUMBNAIL_URI)

            Contact(
                    id = id,
                    name = name,
                    image = thumbnailUri
            )
        } ?: emptyList()
    }

    override fun getContactDetails(lookup: String): Deferred<List<ContactDetails>?> = GlobalScope.async(context = Dispatchers.IO) {
        val accounts = AccountManager.get(context).accounts.map { systemAccount ->
            ContactAccount(type = systemAccount.type, name = systemAccount.name)
        }.toMutableList().also {
            it.add(ContactAccount(null, null))
        }

        accounts.reversed().mapNotNull { account ->
            getContactData(lookup = lookup, accountType = account.type, accountName = account.name)?.takeIf { cursor ->
                cursor.count > 0
            }?.let { cursor ->
                account to cursor
            }?.takeIf { (_, cursor) ->
                cursor.count > 0
            }?.let { (account, cursor) ->
                getDetails(IterableCursor(cursor), account)
            }
        }.takeIf(List<ContactDetails>::isNotEmpty)
    }

    override fun getContactByLookup(lookup: String): Deferred<Contact?> = GlobalScope.async(context = Dispatchers.IO) {

        getContact(lookup)?.takeIf(Cursor::moveToFirst)?.let { cursor ->
            val imageUri = cursor.getStringValueOrNull(ContactsContract.Contacts.PHOTO_URI)
            val name = cursor.getStringValue(ContactsContract.Contacts.DISPLAY_NAME)

            Contact(id = lookup, name = name, image = imageUri)
        }
    }

    @SuppressLint("Recycle")
    private fun getContactData(lookup: String, accountType: String?, accountName: String?): Cursor? =
            if (accountName == null && accountType == null) {
                context.contentResolver.query(ContactsContract.Data.CONTENT_URI,
                        DETAILS_PROJECTION, DATA_DETAILS_SELECTION_DEFAULT_ACCOUNT, arrayOf(lookup), null)
            } else {
                context.contentResolver.query(ContactsContract.Data.CONTENT_URI,
                        DETAILS_PROJECTION, DATA_DETAILS_SELECTION, arrayOf(lookup, accountType, accountName), null)
            }

    /**
     *  ContactsContract.RawContacts.CONTENT_URI
     *  _id - individual for each entry
     *  contact_id - same for all entries with this lookup (contact)
     *  raw_contact_id - same for all entries with this account type
     */

    private fun getContact(lookup: String): Cursor? = context.contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
            CONTACT_PROJECTION, LOOKUP_SELECTION, arrayOf(lookup), null)


    private fun getAllContacts(): Cursor? = context.contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
            PROJECTION, null, null, "${ContactsContract.Data.DISPLAY_NAME_PRIMARY} ASC")

    private fun getDetails(data: IterableCursor, contactAccount: ContactAccount): ContactDetails {

        var name: String? = null
        var note: String? = null
        var photoUri: String? = null
        val phones = mutableListOf<ContactPhone>()
        val emails = mutableListOf<ContactEmail>()
        val addresses = mutableListOf<ContactAddress>()
        val relations = mutableListOf<ContactRelation>()
        val websites = mutableListOf<String>()

        data.forEach { cursor ->
            val dataType = cursor.getStringValue(ContactsContract.Data.MIMETYPE)

            when (dataType) {
                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE -> name = extractName(cursor)
                ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE -> note = extractNote(cursor)
                ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE -> photoUri = extractPhotoUri(cursor)
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE -> phones.add(extractPhone(cursor))
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE -> emails.add(extractEmail(cursor))
                ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE -> addresses.add(extractAddress(cursor))
                ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE -> relations.add(extractRelation(cursor))
                ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE -> websites.add(extractWebsite(cursor))
            }
        }
        return ContactDetails(
                account = contactAccount,
                userName = name,
                photoUri = photoUri,
                phones = phones.toList(),
                emails = emails.toList(),
                addresses = addresses.toList(),
                relations = relations.toList(),
                websites = websites.toList(),
                note = note
        )
    }

    private fun extractPhone(cursor: Cursor): ContactPhone {
        val phone = cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Phone.NUMBER).orEmpty()
        val type = cursor.getIntValue(ContactsContract.CommonDataKinds.Phone.TYPE)

        val defaultLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(type).let(context::getString)

        val label = if (type == ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM) {
            cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Phone.LABEL)
                    ?: defaultLabel
        } else {
            defaultLabel
        }

        val isPrimary = cursor.getIntValue(ContactsContract.CommonDataKinds.Phone.IS_PRIMARY) != 0

        return ContactPhone(phone = phone, type = label, isPrimary = isPrimary)
    }

    private fun extractEmail(cursor: Cursor): ContactEmail {
        val email = cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Email.ADDRESS).orEmpty()
        val type = cursor.getIntValue(ContactsContract.CommonDataKinds.Email.TYPE)


        val defaultLabel = ContactsContract.CommonDataKinds.Email.getTypeLabelResource(type).let(context::getString)

        val label = if (type == ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM) {
            cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Email.LABEL)
                    ?: defaultLabel
        } else {
            defaultLabel
        }

        return ContactEmail(email = email, type = label)
    }

    private fun extractAddress(cursor: Cursor): ContactAddress {
        val address = cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS)
        val type = cursor.getIntValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE)
        val typeName = ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabelResource(type).let(context::getString)

        return ContactAddress(address = address, type = typeName)
    }

    private fun extractRelation(cursor: Cursor): ContactRelation {
        val name = cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Relation.NAME).orEmpty()
        val type = cursor.getIntValue(ContactsContract.CommonDataKinds.Relation.TYPE)

        val typeName = ContactsContract.CommonDataKinds.Relation.getTypeLabelResource(type).let(context::getString)

        return ContactRelation(name = name, type = typeName)
    }

    private fun extractName(cursor: Cursor): String? = cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)

    private fun extractNote(cursor: Cursor): String? = cursor.getStringValueOrNull(ContactsContract.CommonDataKinds.Note.NOTE)

    private fun extractWebsite(cursor: Cursor): String = cursor.getStringValue(ContactsContract.CommonDataKinds.Website.URL)

    private fun extractPhotoUri(cursor: Cursor): String = cursor.getStringValue(ContactsContract.Data.PHOTO_URI)


    companion object {

        private val CONTACT_PROJECTION = arrayOf(
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Contacts.PHOTO_URI,
                ContactsContract.Contacts.DISPLAY_NAME
        )

        private val PROJECTION = arrayOf(
                ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI
        )

        private val DETAILS_PROJECTION = arrayOf(
                ContactsContract.Data._ID,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.Data.MIMETYPE,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.RawContacts.ACCOUNT_TYPE,
                ContactsContract.RawContacts.ACCOUNT_NAME,
                ContactsContract.Data.PHOTO_URI,
                ContactsContract.CommonDataKinds.Phone.IS_PRIMARY,
                ContactsContract.Data.IS_SUPER_PRIMARY,

                ContactsContract.Data.DATA1,
                ContactsContract.Data.DATA2,
                ContactsContract.Data.DATA3
        )

        private const val LOOKUP_SELECTION = "${ContactsContract.Data.LOOKUP_KEY} = ?"
        private const val DATA_DETAILS_SELECTION_DEFAULT_ACCOUNT = "${ContactsContract.Data.LOOKUP_KEY} = ? AND ${ContactsContract.RawContacts.ACCOUNT_TYPE} IS NULL AND ${ContactsContract.RawContacts.ACCOUNT_NAME} IS NULL"
        private const val DATA_DETAILS_SELECTION = "${ContactsContract.Data.LOOKUP_KEY} = ? AND ${ContactsContract.RawContacts.ACCOUNT_TYPE} = ? AND ${ContactsContract.RawContacts.ACCOUNT_NAME} = ?"
    }

}
