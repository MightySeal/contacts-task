package io.konstantinzolotov.data.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.konstantinzolotov.domain.repositories.ContactsRepository
import io.konstantinzolotov.domain.repositories.PermissionsRepository

@Component(modules = [DataModule::class])
interface DataComponent {

    fun exposeContactsRepository(): ContactsRepository

    fun exposePermissionsRepository(): PermissionsRepository

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun withContext(context: Context): Builder

        fun build(): DataComponent
    }
}