package io.konstantinzolotov.data.di

import dagger.Module
import dagger.Provides
import io.konstantinzolotov.data.ContactsRepositoryImpl
import io.konstantinzolotov.data.PermissionsRepositoryImpl
import io.konstantinzolotov.domain.repositories.ContactsRepository
import io.konstantinzolotov.domain.repositories.PermissionsRepository

@Module
open class DataModule {

    @Provides
    fun provideContactsRepository(repo: ContactsRepositoryImpl): ContactsRepository = repo

    @Provides
    fun providePermissionRepository(repo: PermissionsRepositoryImpl): PermissionsRepository = repo

    /*@Binds
    abstract fun bindContactsRepository(repo: ContactsRepositoryImpl): ContactsRepository*/

}