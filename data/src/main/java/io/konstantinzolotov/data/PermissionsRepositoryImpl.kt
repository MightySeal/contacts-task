package io.konstantinzolotov.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import io.konstantinzolotov.domain.repositories.PermissionsRepository
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.async
import javax.inject.Inject


class PermissionsRepositoryImpl @Inject constructor(private val context: Context) : PermissionsRepository {

    override fun hasContactsReadPermission(): Deferred<Boolean> = GlobalScope.async {
        ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
    }

}