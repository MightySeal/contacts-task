package io.konstantinzolotov.data

import android.database.Cursor


internal fun Cursor.asIterable() = IterableCursor(this)

internal fun Cursor.getStringValue(columnName: String): String = this.getString(this.getColumnIndex(columnName))
internal fun Cursor.getStringValueOrNull(columnName: String): String? = this.getColumnIndex(columnName).takeIf { index -> index > -1 }
        ?.let { index -> this.getString(index) }

internal fun Cursor.getLongValue(columnName: String): Long = this.getLong(this.getColumnIndex(columnName))
internal fun Cursor.getIntValue(columnName: String): Int = this.getInt(this.getColumnIndex(columnName))

internal class IterableCursor(private val cursor: Cursor) : Iterable<Cursor> {
    override fun iterator(): Iterator<Cursor> = object : Iterator<Cursor> {
        override fun hasNext(): Boolean = !cursor.isClosed && !cursor.isAfterLast && cursor.moveToNext()

        override fun next(): Cursor = cursor
    }
}
