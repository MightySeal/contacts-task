package io.konstantinzolotov.contapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import io.konstantinzolotov.contapp.contact.ContactDetailsViewModel
import io.konstantinzolotov.contapp.list.ContactRow
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.models.Contact
import io.konstantinzolotov.domain.models.ContactAccount
import io.konstantinzolotov.domain.models.ContactDetails
import kotlinx.coroutines.experimental.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class ContactDetailsViewModelTest {

    @Mock
    lateinit var interactor: ContactsInteractor

    @Spy
    lateinit var detailsObserver: Observer<List<ContactRow>?>

    @Spy
    lateinit var contactObserver: Observer<Contact?>

    @Spy
    lateinit var permissionObserver: Observer<Boolean?>

    @Mock
    lateinit var application: Application

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        whenever(application.getString(any())).thenReturn(STRING_STUB)
    }

    @Test
    fun testHasPermission() = runBlocking {
        val permission = GlobalScope.async { true }
        val details = GlobalScope.async {
            listOf(CONTACT_DETAILS_STUB)
        }
        val avatar = GlobalScope.async {
            CONTACT_STUB
        }

        val jobs = mutableListOf<Job>()

        whenever(interactor.getPermission()).thenReturn(permission.also { jobs.add(it) })
        whenever(interactor.getContactDetails(any())).thenReturn(details.also { jobs.add(it) })
        whenever(interactor.getContact(any())).thenReturn(avatar.also { jobs.add(it) })

        val viewmodel = ContactDetailsViewModel(interactor, VALID_LOOKUP, application, Dispatchers.Unconfined)

        jobs.joinAll()

        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getDetails().observeForever(detailsObserver)
        viewmodel.getContact().observeForever(contactObserver)

        assert(viewmodel.getPermission().value == true)
        assert(viewmodel.getContact().value == CONTACT_STUB)
        assert(viewmodel.getDetails().value?.let { actual ->
            actual.size == 2 && actual[1] == ContactRow(icon = null, title = STRING_STUB, value = "Test name")
        } ?: false)
    }


    @Test
    fun testNoPermission() = runBlocking {

        val permission = GlobalScope.async { false }
        whenever(interactor.getPermission()).thenReturn(permission)

        val viewmodel = ContactDetailsViewModel(interactor, VALID_LOOKUP, application, Dispatchers.Unconfined)

        permission.join()


        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getDetails().observeForever(detailsObserver)
        viewmodel.getContact().observeForever(contactObserver)

        verify(permissionObserver).onChanged(argThat {
            !this
        })
        verifyZeroInteractions(detailsObserver)
        verifyZeroInteractions(contactObserver)
    }

    @Test
    fun testNoAvatar() = runBlocking {

        val permission = GlobalScope.async { true }
        val details = GlobalScope.async {
            emptyList<ContactDetails>()
        }
        val contact = GlobalScope.async { null }

        whenever(interactor.getPermission()).thenReturn(permission)

        whenever(interactor.getContactDetails(any())).thenReturn(details)

        whenever(interactor.getContact(any())).thenReturn(contact)

        val viewmodel = ContactDetailsViewModel(interactor, VALID_LOOKUP, application, Dispatchers.Unconfined)

        permission.join()
        details.join()
        contact.join()

        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getDetails().observeForever(detailsObserver)
        viewmodel.getContact().observeForever(contactObserver)

        verify(permissionObserver).onChanged(argThat {
            this
        })

        verify(detailsObserver).onChanged(argThat {
            this.isEmpty()
        })

        verify(contactObserver).onChanged(null)

    }

    @Test
    fun testInvalidLookup() = runBlocking {
        val permission = GlobalScope.async { true }
        val details = GlobalScope.async { null }
        val contact = GlobalScope.async { null }

        whenever(interactor.getPermission()).thenReturn(permission)

        whenever(interactor.getContactDetails(any())).thenReturn(details)

        whenever(interactor.getContact(any())).thenReturn(contact)

        val viewmodel = ContactDetailsViewModel(interactor, INVALID_LOOKUP, application, Dispatchers.Unconfined)

        permission.join()
        details.join()
        contact.join()

        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getDetails().observeForever(detailsObserver)
        viewmodel.getContact().observeForever(contactObserver)

        verify(permissionObserver).onChanged(true)
        verify(detailsObserver).onChanged(null)
        verify(contactObserver).onChanged(null)
    }

    companion object {
        private const val VALID_LOOKUP = "valid_lookup"
        private const val INVALID_LOOKUP = "invalid_lookup"
        private const val AVATAR_URI = "content://com.android.contacts/display_photo/1"
        private const val STRING_STUB = "string stub"

        private val CONTACT_DETAILS_STUB = ContactDetails(ContactAccount(null, null), "Test name")
        private val CONTACT_STUB = Contact(VALID_LOOKUP, AVATAR_URI, "Test name")
    }


}