package io.konstantinzolotov.contapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import io.konstantinzolotov.contapp.list.ContactsListViewModel
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.models.Contact
import kotlinx.coroutines.experimental.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ContactsListViewModelTest {

    @Mock
    lateinit var interactor: ContactsInteractor

    @Spy
    lateinit var contactsObserver: Observer<List<Contact>?>

    @Spy
    lateinit var permissionObserver: Observer<Boolean?>

    @Mock
    lateinit var application: Application

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testHasPermission() = runBlocking {

        val permission = GlobalScope.async { true }
        whenever(interactor.getPermission()).thenReturn(permission)

        val contacts = GlobalScope.async { emptyList<Contact>() }
        whenever(interactor.getContacts()).thenReturn(contacts)

        val viewmodel = ContactsListViewModel(interactor, application, Dispatchers.Unconfined)

        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getContacts().observeForever(contactsObserver)

        permission.join()
        contacts.join()

        verify(contactsObserver).onChanged(any())
        assert(viewmodel.getContacts().value?.isEmpty() == true)
        assert(viewmodel.getPermission().value == true)
    }

    @Test
    fun testNoPermission() = runBlocking {

        val permission = GlobalScope.async { false }
        whenever(interactor.getPermission()).thenReturn(permission)

        val viewmodel = ContactsListViewModel(interactor, application, Dispatchers.Unconfined)

        permission.join()

        viewmodel.getPermission().observeForever(permissionObserver)
        viewmodel.getContacts().observeForever(contactsObserver)

        assert(viewmodel.getPermission().value == false)
        verifyZeroInteractions(contactsObserver)
        assert(viewmodel.getContacts().value == null)
    }


}