package io.konstantinzolotov.contapp.contact.di

import dagger.Module
import dagger.Provides
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.interactors.ContactsInteractorImpl

@Module
open class ContactScreenModule {

    @Provides
    @ContactDetailsComponent.ContactDetailsScope
    fun bindInteractor(interactor: ContactsInteractorImpl): ContactsInteractor = interactor

    annotation class Lookup
}