package io.konstantinzolotov.contapp.contact

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.transition.ArcMotion
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.konstantinzolotov.contapp.R
import io.konstantinzolotov.contapp.app.App
import io.konstantinzolotov.contapp.common.recycler.MarginDecoration
import io.konstantinzolotov.contapp.common.setVisibility
import kotlinx.android.synthetic.main.activity_contact_details.*
import kotlinx.android.synthetic.main.contact_avatar.*
import kotlinx.android.synthetic.main.permission_layout.*
import javax.inject.Inject


class ContactDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: DetailsVmFactory

    private lateinit var viewModel: ContactDetailsViewModel

    private val adapter = ContactDetailsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)

        postponeEnterTransition()
        window.sharedElementEnterTransition?.pathMotion = ArcMotion()


        if (!isChangingConfigurations) {
            App.get(this).appComponent
                    .contactDetailsScreenComponent()
                    .contactLookup(intent.getStringExtra(CONTACT_LOOKUP_KEY).orEmpty())
                    .build()
                    .inject(this)
        }

        viewModel = ViewModelProviders.of(this, factory).get(ContactDetailsViewModel::class.java)

        contactDetails.layoutManager = LinearLayoutManager(this)
        contactDetails.addItemDecoration(MarginDecoration(context = this, forType = ContactDetailsAdapter.TITLE_TYPE))
        contactDetails.addItemDecoration(MarginDecoration(
                topMarginRes = R.dimen.title_margin_top,
                context = this,
                forType = ContactDetailsAdapter.TITLE_TYPE))

        contactDetails.addItemDecoration(MarginDecoration(
                bottomMarginRes = R.dimen.details_item_margin,
                topMarginRes = R.dimen.details_item_margin,
                context = this,
                forType = ContactDetailsAdapter.ITEM_TYPE))

        contactDetails.adapter = adapter

        viewModel.getDetails().observe(this, Observer { details ->
            details?.let(adapter::replace)
        })

        viewModel.getContact().observe(this, Observer { contact ->
            contact?.let {
                if (it.image != null) {
                    photoThumbnail.setVisibility(true)
                    contactInitials.setVisibility(false)
                    photoThumbnail.setImageURI(Uri.parse(it.image))
                } else {
                    photoThumbnail.setVisibility(false)
                    contactInitials.setVisibility(true)
                    contactInitials.text = it.name.first().toString()
                }
            }
            startPostponedEnterTransition()
        })

        viewModel.getPermission().observe(this, Observer<Boolean> { hasPermission ->

            if (hasPermission) {
                noPermissionLayout.setVisibility(false)
                contactDetails.setVisibility(true)
                avatarContainer.setVisibility(true)
            } else {
                noPermissionLayout.setVisibility(true)
                contactDetails.setVisibility(false)
                avatarContainer.setVisibility(false)
                requestContactPermission()
            }
        })

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CONTACTS_REQUEST_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            viewModel.permissionGranted()
        }
    }

    private fun requestContactPermission() = ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), CONTACTS_REQUEST_CODE)

    companion object {
        private const val CONTACTS_REQUEST_CODE = 321

        private const val CONTACT_LOOKUP_KEY = "contact_lookup"

        fun createIntent(context: Context, lookup: String) = Intent(context, ContactDetailsActivity::class.java).apply {
            putExtra(CONTACT_LOOKUP_KEY, lookup)
        }
    }
}