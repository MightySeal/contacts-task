package io.konstantinzolotov.contapp.contact

import android.content.Context
import android.view.View
import io.konstantinzolotov.contapp.R
import io.konstantinzolotov.contapp.common.BaseAdapter
import io.konstantinzolotov.contapp.list.ContactRow


class ContactDetailsAdapter(context: Context) : BaseAdapter<ContactRow, DetailsViewHolder>(context) {

    override fun getLayoutId(viewType: Int): Int = viewType

    override fun getItemViewType(position: Int): Int = if (data[position].isTitleItem) TITLE_TYPE else ITEM_TYPE

    override fun createViewHolder(view: View, viewType: Int): DetailsViewHolder =
            if (viewType == TITLE_TYPE) {
                ContactTitleViewHolder(view)
            } else {
                ContactDetailsViewHolder(view)
            }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.value.text = data[position].title
        holder.description.text = data[position].value
    }


    companion object {
        const val TITLE_TYPE = R.layout.list_item_details_title
        const val ITEM_TYPE = R.layout.list_item_details
    }


}