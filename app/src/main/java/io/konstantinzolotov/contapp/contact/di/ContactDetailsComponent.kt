package io.konstantinzolotov.contapp.contact.di

import dagger.BindsInstance
import dagger.Subcomponent
import io.konstantinzolotov.contapp.contact.ContactDetailsActivity
import javax.inject.Qualifier
import javax.inject.Scope

@ContactDetailsComponent.ContactDetailsScope
@Subcomponent(modules = [ContactScreenModule::class])
interface ContactDetailsComponent {

    fun inject(activity: ContactDetailsActivity)

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun contactLookup(@ContactLookup lookup: String): Builder

        fun build(): ContactDetailsComponent
    }


    @Qualifier
    annotation class ContactLookup

    @Scope
    annotation class ContactDetailsScope
}