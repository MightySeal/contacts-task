package io.konstantinzolotov.contapp.contact

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.konstantinzolotov.contapp.R
import io.konstantinzolotov.contapp.common.BaseViewModel
import io.konstantinzolotov.contapp.list.ContactRow
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.models.Contact
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.launch

class ContactDetailsViewModel constructor(
        private val interactor: ContactsInteractor,
        private val lookup: String,
        app: Application,
        dispatcher: CoroutineDispatcher
) : BaseViewModel(dispatcher, app) {

    private val contactDetails = MutableLiveData<List<ContactRow>>()
    private val contact = MutableLiveData<Contact?>()
    private val permission = MutableLiveData<Boolean>()

    init {
        reloadContactData(lookup)
    }

    fun getDetails(): LiveData<List<ContactRow>?> = contactDetails
    fun getContact(): LiveData<Contact?> = contact
    fun getPermission(): LiveData<Boolean> = permission

    fun permissionGranted() {
        permission.value = true
        reloadContactData(lookup)
    }

    private fun reloadContactData(lookup: String) {
        launch {
            val permissionGranted = interactor.getPermission()

            if (permissionGranted.await()) {
                val rows = getContactData(lookup)
                val avatar = interactor.getContact(lookup)

                permission.value = true
                contact.value = avatar.await()
                contactDetails.value = rows
            } else {
                permission.value = false
            }
        }
    }

    private suspend fun getContactData(lookup: String): List<ContactRow>? = interactor.getContactDetails(lookup).await()?.flatMap { details ->

        val accountRow = ContactRow(title = (details.account.name
                ?: getString(R.string.local_account)), isTitleItem = true)
        val nameRow = details.userName?.let { ContactRow(title = getString(R.string.name), value = it) }
        val phoneRows = details.phones.map { phone -> ContactRow(title = phone.type, value = phone.phone) }
        val emailRows = details.emails.map { email -> ContactRow(title = email.type, value = email.email) }
        val addressRows = details.addresses.map { address -> ContactRow(title = address.type, value = address.address) }
        val relationRows = details.relations.map { relations -> ContactRow(title = relations.type, value = relations.name) }
        val websiteRows = details.websites.map { website -> ContactRow(title = getString(R.string.website), value = website) }
        val noteRow = details.note?.let { ContactRow(title = it) }

        listOfNotNull(
                accountRow,
                nameRow,
                *phoneRows.toTypedArray(),
                *emailRows.toTypedArray(),
                *addressRows.toTypedArray(),
                *relationRows.toTypedArray(),
                *websiteRows.toTypedArray(),
                noteRow
        )
    }
}
