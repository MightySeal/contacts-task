package io.konstantinzolotov.contapp.contact

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.konstantinzolotov.contapp.contact.di.ContactDetailsComponent
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import kotlinx.coroutines.experimental.CoroutineDispatcher
import javax.inject.Inject

class DetailsVmFactory @Inject constructor(
        private val interactor: ContactsInteractor,
        @ContactDetailsComponent.ContactLookup private val lookup: String,
        private val app: Application,
        private val dispatcher: CoroutineDispatcher
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when (modelClass) {
        ContactDetailsViewModel::class.java -> ContactDetailsViewModel(interactor, lookup, app, dispatcher) as T
        else -> throw IllegalArgumentException("No viewmodel registered for class $modelClass")
    }
}