package io.konstantinzolotov.contapp.contact

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_details.view.*

sealed class DetailsViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    abstract val value: TextView
    abstract val description: TextView
}

class ContactDetailsViewHolder(item: View) : DetailsViewHolder(item) {
    override val value: TextView = item.value
    override val description: TextView = item.description
}

class ContactTitleViewHolder(item: View) : DetailsViewHolder(item) {
    override val value: TextView = item.value
    override val description: TextView = item.description
}