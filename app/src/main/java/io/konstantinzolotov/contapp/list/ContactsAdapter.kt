package io.konstantinzolotov.contapp.list

import android.content.Context
import android.net.Uri
import android.view.View
import io.konstantinzolotov.contapp.R
import io.konstantinzolotov.contapp.common.BaseAdapter
import io.konstantinzolotov.contapp.common.setVisibility
import io.konstantinzolotov.domain.models.Contact

class ContactsAdapter(context: Context,
                      private val clickListener: (lookup: String, animateView: View) -> Unit
) : BaseAdapter<Contact, ContactViewHolder>(context) {

    override fun getLayoutId(viewType: Int): Int = R.layout.list_item_contact

    override fun createViewHolder(view: View, viewType: Int): ContactViewHolder = ContactViewHolder(view).apply {
        view.setOnClickListener { _ ->
            clickListener(data[this.adapterPosition].id, this.avatarContainer)
        }
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = data[position]
        holder.name.text = contact.name


        if (contact.image != null) {
            holder.image.setVisibility(true)
            holder.fallback.setVisibility(false)
            holder.image.setImageURI(Uri.parse(contact.image))
        } else {
            holder.image.setVisibility(false)
            holder.fallback.setVisibility(true)

            holder.fallback.text = contact.name.first().toString()
        }
    }
}