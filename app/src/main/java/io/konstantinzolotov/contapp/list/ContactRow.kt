package io.konstantinzolotov.contapp.list


data class ContactRow(
        val icon: Int? = null,
        val title: String,
        val value: String? = null,
        val isTitleItem: Boolean = false
)