package io.konstantinzolotov.contapp.list.di

import dagger.Subcomponent
import io.konstantinzolotov.contapp.list.ContactsListActivity
import javax.inject.Scope

@ContactsListComponent.ContactsListScope
@Subcomponent(modules = [ContactsListModule::class])
interface ContactsListComponent {

    fun inject(activity: ContactsListActivity)

    @Subcomponent.Builder
    interface Builder {

        fun build(): ContactsListComponent
    }

    @Scope
    annotation class ContactsListScope
}