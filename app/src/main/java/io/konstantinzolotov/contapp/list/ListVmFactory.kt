package io.konstantinzolotov.contapp.list

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import kotlinx.coroutines.experimental.CoroutineDispatcher
import javax.inject.Inject

class ListVmFactory @Inject constructor(
        private val interactor: ContactsInteractor,
        private val app: Application,
        private val dispatcher: CoroutineDispatcher
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when (modelClass) {
        ContactsListViewModel::class.java -> ContactsListViewModel(interactor, app, dispatcher) as T
        else -> throw IllegalArgumentException("No viewmodel registered for class $modelClass")
    }
}