package io.konstantinzolotov.contapp.list

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.konstantinzolotov.contapp.common.BaseViewModel
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.models.Contact
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.launch


class ContactsListViewModel constructor(
        private val interactor: ContactsInteractor,
        app: Application,
        dispatcher: CoroutineDispatcher
) : BaseViewModel(dispatcher, app) {

    private val contacts = MutableLiveData<List<Contact>>()
    private val permission = MutableLiveData<Boolean>()

    init {
        reloadContacts()
    }

    fun getContacts(): LiveData<List<Contact>> = contacts
    fun getPermission(): LiveData<Boolean> = permission

    fun permissionGranted() {
        permission.value = true
        reloadContacts()
    }

    private fun reloadContacts() {

        launch {
            val permissionGranted = interactor.getPermission()

            if (permissionGranted.await()) {
                permission.value = true
                contacts.value = interactor.getContacts().await()
            } else {
                permission.value = false
            }
        }
    }

}