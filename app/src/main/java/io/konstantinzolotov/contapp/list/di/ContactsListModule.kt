package io.konstantinzolotov.contapp.list.di

import dagger.Module
import dagger.Provides
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.interactors.ContactsInteractorImpl

@Module
class ContactsListModule {

    @Provides
    @ContactsListComponent.ContactsListScope
    fun bindInteractor(interactor: ContactsInteractorImpl): ContactsInteractor = interactor

}