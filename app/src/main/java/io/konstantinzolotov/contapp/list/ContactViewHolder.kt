package io.konstantinzolotov.contapp.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.contact_avatar.view.*
import kotlinx.android.synthetic.main.list_item_contact.view.*


class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val name: TextView = view.contactName
    val image: ImageView = view.photoThumbnail
    val fallback: TextView = view.contactInitials
    val avatarContainer: View = view.avatarContainer
}