package io.konstantinzolotov.contapp.list

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.konstantinzolotov.contapp.R
import io.konstantinzolotov.contapp.app.App
import io.konstantinzolotov.contapp.common.recycler.MarginDecoration
import io.konstantinzolotov.contapp.common.setVisibility
import io.konstantinzolotov.contapp.contact.ContactDetailsActivity
import io.konstantinzolotov.domain.models.Contact
import kotlinx.android.synthetic.main.activity_contacts_list.*
import kotlinx.android.synthetic.main.no_content_layout.*
import kotlinx.android.synthetic.main.permission_layout.*
import javax.inject.Inject


class ContactsListActivity : AppCompatActivity() {


    @Inject
    lateinit var factory: ListVmFactory

    private lateinit var viewModel: ContactsListViewModel

    private val adapter = ContactsAdapter(context = this,
            clickListener = this::openContactDetails)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts_list)

        postponeEnterTransition()
        if (!isChangingConfigurations) {
            App.get(this).appComponent
                    .contactListScreenComponent()
                    .build()
                    .inject(this)
        }

        viewModel = ViewModelProviders.of(this, factory).get(ContactsListViewModel::class.java)

        contactsList.layoutManager = LinearLayoutManager(this)
        contactsList.addItemDecoration(MarginDecoration(bottomMarginRes = R.dimen.default_recycler_item_spacing, context = this))
        contactsList.adapter = adapter

        viewModel.getContacts().observe(this, Observer<List<Contact>> { contacts ->
            contacts?.let {
                if (it.isEmpty()) {
                    noContentLayout.setVisibility(true)
                    contactsList.setVisibility(false)
                } else {
                    noContentLayout.setVisibility(false)
                    contactsList.setVisibility(true)
                    adapter.replace(it)
                }
                startPostponedEnterTransition()
            }
        })

        viewModel.getPermission().observe(this, Observer<Boolean> { hasPermission ->

            if (hasPermission) {
                noPermissionLayout.setVisibility(false)
                contactsList.setVisibility(true)
            } else {
                noPermissionLayout.setVisibility(true)
                contactsList.setVisibility(false)
                requestContactPermission()
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CONTACTS_REQUEST_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            viewModel.permissionGranted()
        }
    }

    private fun requestContactPermission() = ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), CONTACTS_REQUEST_CODE)

    private fun openContactDetails(lookup: String, animateView: View) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, animateView, getString(R.string.avatar_transition_name))

        startActivity(ContactDetailsActivity.createIntent(this, lookup), options.toBundle())
    }

    private companion object {
        private const val CONTACTS_REQUEST_CODE = 123
    }

}
