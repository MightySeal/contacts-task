package io.konstantinzolotov.contapp.common.recycler

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView


class MarginDecoration(
        context: Context,
        @DimenRes topMarginRes: Int? = null,
        @DimenRes bottomMarginRes: Int? = null,
        private val forType: Int? = null
) : RecyclerView.ItemDecoration() {

    private val topMarginHeight = topMarginRes?.let { context.resources.getDimensionPixelSize(it) }
    private val bottomMarginHeight = bottomMarginRes?.let { context.resources.getDimensionPixelSize(it) }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        parent.adapter?.let { adapter ->
            val itemPosition = parent.getChildAdapterPosition(view)

            if (forType == null || parent.adapter?.getItemViewType(itemPosition) == forType) {
                val totalCount = adapter.itemCount
                if (itemPosition >= 0 && itemPosition < totalCount) {
                    topMarginHeight?.let { outRect.top = it }
                    bottomMarginHeight?.let { outRect.bottom = it }
                }
            }
        }
    }
}