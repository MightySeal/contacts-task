package io.konstantinzolotov.contapp.common

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job
import kotlin.coroutines.experimental.CoroutineContext


abstract class BaseViewModel(
        private val dispatcher: CoroutineDispatcher,
        private val app: Application
) : AndroidViewModel(app), CoroutineScope {

    private val jobsList = mutableListOf<Job>()

    override val coroutineContext: CoroutineContext
        get() = untilCleared() + dispatcher

    override fun onCleared() {
        super.onCleared()
        jobsList.onEach { job -> job.cancel() }.clear()
    }

    protected fun getString(@StringRes resId: Int) = app.getString(resId)

    private fun untilCleared() = Job().apply {
        jobsList.add(this)
    }
}