package io.konstantinzolotov.contapp.common

import android.view.View


fun View.setVisibility(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

