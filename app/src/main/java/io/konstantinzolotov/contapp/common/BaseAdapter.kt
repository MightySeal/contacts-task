package io.konstantinzolotov.contapp.common

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


abstract class BaseAdapter<D, VH : RecyclerView.ViewHolder>(context: Context) : RecyclerView.Adapter<VH>() {

    protected val data = mutableListOf<D>()
    private val inflater by lazy(LazyThreadSafetyMode.NONE) {
        LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
            createViewHolder(inflater.inflate(getLayoutId(viewType), parent, false), viewType)

    fun replace(newData: List<D>) = data.run {
        clear()
        addAll(newData)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    abstract fun getLayoutId(viewType: Int): Int

    abstract fun createViewHolder(view: View, viewType: Int): VH

}