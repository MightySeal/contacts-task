package io.konstantinzolotov.contapp.app

import android.app.Application
import android.content.Context
import io.konstantinzolotov.contapp.app.di.AppComponent
import io.konstantinzolotov.contapp.app.di.AppModule
import io.konstantinzolotov.contapp.app.di.DaggerAppComponent
import io.konstantinzolotov.data.di.DaggerDataComponent
import timber.log.Timber


class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .dataComponent(DaggerDataComponent.builder().withContext(this).build())
                .build()
    }

    companion object {
        fun get(context: Context): App = context.applicationContext as App
    }
}