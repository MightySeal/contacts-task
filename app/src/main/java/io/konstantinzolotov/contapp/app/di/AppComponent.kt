package io.konstantinzolotov.contapp.app.di

import dagger.Component
import io.konstantinzolotov.contapp.contact.di.ContactDetailsComponent
import io.konstantinzolotov.contapp.list.di.ContactsListComponent
import io.konstantinzolotov.data.di.DataComponent
import javax.inject.Singleton

@Singleton
@Component(
        dependencies = [DataComponent::class],
        modules = [AppModule::class])
interface AppComponent {

    fun contactDetailsScreenComponent(): ContactDetailsComponent.Builder
    fun contactListScreenComponent(): ContactsListComponent.Builder
}