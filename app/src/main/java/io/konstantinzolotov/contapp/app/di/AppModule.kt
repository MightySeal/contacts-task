package io.konstantinzolotov.contapp.app.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.konstantinzolotov.contapp.contact.di.ContactDetailsComponent
import io.konstantinzolotov.contapp.list.di.ContactsListComponent
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.android.Main

@Module(
        subcomponents = [ContactsListComponent::class, ContactDetailsComponent::class]
)
open class AppModule(private val app: Application) {

    @Provides
    fun provideAppContext(): Context = app.applicationContext

    @Provides
    fun provideApplication(): Application = app

    @Provides
    fun provideDispatcher(): CoroutineDispatcher = Dispatchers.Main

}