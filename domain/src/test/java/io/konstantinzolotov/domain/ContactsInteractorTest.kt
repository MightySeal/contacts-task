package io.konstantinzolotov.domain

import com.nhaarman.mockito_kotlin.whenever
import io.konstantinzolotov.domain.interactors.ContactsInteractor
import io.konstantinzolotov.domain.interactors.ContactsInteractorImpl
import io.konstantinzolotov.domain.models.Contact
import io.konstantinzolotov.domain.repositories.ContactsRepository
import io.konstantinzolotov.domain.repositories.PermissionsRepository
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class ContactsInteractorTest {

    @Mock
    lateinit var permissionRepoistory: PermissionsRepository

    @Mock
    lateinit var contactsRepoistory: ContactsRepository

    lateinit var interactor: ContactsInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        whenever(permissionRepoistory.hasContactsReadPermission()).then {
            GlobalScope.async { true }
        }

        whenever(contactsRepoistory.getContactByLookup(INVALID_LOOKUP)).then {
            GlobalScope.async { null }
        }

        whenever(contactsRepoistory.getContactByLookup(VALID_LOOKUP)).then {
            GlobalScope.async { Contact(VALID_LOOKUP, NAME, AVATAR_VALUE) }
        }

        whenever(contactsRepoistory.getAll()).then {
            GlobalScope.async { emptyList<Contact>()}
        }

        interactor = ContactsInteractorImpl(permissionRepoistory, contactsRepoistory)
    }


    @Test
    fun testInvalidAvatar() = runBlocking {
        val avatar = interactor.getContact(INVALID_LOOKUP).await()
        assert(avatar == null)
    }

    @Test
    fun testValidAvatar() = runBlocking {
        val contact = interactor.getContact(VALID_LOOKUP).await()
        assert(contact == Contact(VALID_LOOKUP, NAME, AVATAR_VALUE))
    }

    @Test
    fun testContacts() = runBlocking {
        val contacts = interactor.getContacts().await()
        assert(contacts.isEmpty())
    }


    companion object {
        private const val INVALID_LOOKUP = "invalid_lookup"
        private const val VALID_LOOKUP = "valid_lookup"
        private const val AVATAR_VALUE = "avatar"
        private const val NAME = "name"
    }

}