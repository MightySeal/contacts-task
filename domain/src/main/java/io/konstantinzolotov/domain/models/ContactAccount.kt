package io.konstantinzolotov.domain.models


data class ContactAccount(val type: String?, val name: String?)