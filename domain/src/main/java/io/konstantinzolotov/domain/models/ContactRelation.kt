package io.konstantinzolotov.domain.models


data class ContactRelation(val name: String, val type: String)