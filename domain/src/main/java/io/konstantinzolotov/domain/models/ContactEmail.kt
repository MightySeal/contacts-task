package io.konstantinzolotov.domain.models


data class ContactEmail(
        val email: String,
        val type: String
)