package io.konstantinzolotov.domain.models


data class ContactAddress(val address: String, val type: String)