package io.konstantinzolotov.domain.models


data class ContactPhone(
        val phone: String,
        val type: String,
        val isPrimary: Boolean = false
)