package io.konstantinzolotov.domain.models

data class Contact(
        val id: String,
        val name: String,
        val image: String? = null
)