package io.konstantinzolotov.domain.models


data class ContactDetails(
        val account: ContactAccount,
        val userName: String?,
        val photoUri: String? = null,
        val phones: List<ContactPhone> = emptyList(),
        val emails: List<ContactEmail> = emptyList(),
        val addresses: List<ContactAddress> = emptyList(),
        val relations: List<ContactRelation> = emptyList(),
        val websites: List<String> = emptyList(),
        val note: String? = null
)