package io.konstantinzolotov.domain.repositories

import io.konstantinzolotov.domain.models.Contact
import io.konstantinzolotov.domain.models.ContactDetails
import kotlinx.coroutines.experimental.Deferred


interface ContactsRepository {
    fun getAll(): Deferred<List<Contact>>
    fun getContactDetails(lookup: String): Deferred<List<ContactDetails>?>
    fun getContactByLookup(lookup: String): Deferred<Contact?>
}