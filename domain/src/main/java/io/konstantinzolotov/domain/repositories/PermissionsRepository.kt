package io.konstantinzolotov.domain.repositories

import kotlinx.coroutines.experimental.Deferred


interface PermissionsRepository {
    fun hasContactsReadPermission(): Deferred<Boolean>
}