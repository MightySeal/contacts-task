package io.konstantinzolotov.domain.interactors

import io.konstantinzolotov.domain.models.Contact
import io.konstantinzolotov.domain.models.ContactDetails
import io.konstantinzolotov.domain.repositories.ContactsRepository
import io.konstantinzolotov.domain.repositories.PermissionsRepository
import kotlinx.coroutines.experimental.Deferred
import javax.inject.Inject

class ContactsInteractorImpl @Inject constructor(
        private val permissionRepository: PermissionsRepository,
        private val contactsRepository: ContactsRepository) : ContactsInteractor {

    override fun getContacts(): Deferred<List<Contact>> = contactsRepository.getAll()

    override fun getContactDetails(lookup: String): Deferred<List<ContactDetails>?> = contactsRepository.getContactDetails(lookup)

    override fun getContact(lookup: String): Deferred<Contact?> = contactsRepository.getContactByLookup(lookup)

    override fun getPermission(): Deferred<Boolean> = permissionRepository.hasContactsReadPermission()
}