package io.konstantinzolotov.domain.interactors

import io.konstantinzolotov.domain.models.Contact
import io.konstantinzolotov.domain.models.ContactDetails
import kotlinx.coroutines.experimental.Deferred


interface ContactsInteractor {

    fun getContacts(): Deferred<List<Contact>>

    fun getContactDetails(lookup: String): Deferred<List<ContactDetails>?>

    fun getContact(lookup: String): Deferred<Contact?>

    fun getPermission(): Deferred<Boolean>
}